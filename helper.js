'use strict';

var crypto = require('crypto');

exports.pagination = function (req, res) {

	return {
		page: Math.max(0, req.query.page),
		perPage: 5
	}
}

exports.encrypt = function (string) {
	var hashMd5 = crypto.createHash('md5').update(config.salt + string).digest("hex");
	var hashSha1 = crypto.createHash('sha1').update(hashMd5).digest("hex");
	return hashSha1;
}