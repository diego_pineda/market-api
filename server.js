var express = require('express'),
 	app = express(),
 	port = process.env.PORT || 3000;
 	mongoose = require('mongoose'),
 	crypto = require('crypto'),
 	responser = require('./api/controllers/responserController'),
 	morgan = require('morgan'),
 	jwt = require('jsonwebtoken'),
 	bodyParser = require('body-parser'),
 	expressValidator = require('express-validator');

 	//main Configuration
 	config = require('./config'),
 	helper = require('./helper'),

 	//models
 	Business = require('./api/models/businessModel'),
	Product = require('./api/models/productModel'),
	Cart = require('./api/models/cartModel'),
	Order = require('./api/models/orderModel'),
	Customer = require('./api/models/customerModel'),
	User = require('./api/models/userModel'),
	


mongoose.Promise = global.Promise;
mongoose.connect(config.database);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());

// use morgan to log requests to the console
app.use(morgan('dev'));


var routes = require('./api/routes/apiRoutes');
routes(app);

app.get('/', function(req, res) {
    res.send('API is at http://localhost:' + port + '/api');
});

app.get('/setup', function(req, res) {
		// create a sample user
		var nick = new User({ 
			name: 'Default User',
			email: 'demo@maket.com',
			password: helper.encrypt('demopassword'),
			admin: true 
		});

		// save the sample user
		nick.save(function(err) {
		if (err) throw err;

		console.log('User saved successfully');
			res.json({ success: true });
		});
});


app.use(function(req, res) {
	res.status(404).send({url: req.originalUrl + ' not found'})
});



app.listen(port);
console.log('Market RESTful API server started on: ' + port);
