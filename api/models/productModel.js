'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSchema = new Schema({
	name: {
		type: String,
		Required: 'Kindly enter the name of the product',
		default: null
	},
	sku: {
		type: String,
		Required: 'enter the sku of the product',
		default: null
	},
	description: {
		type: String,
		Required: 'Enter the description of the product',
		default: null
	},
	price: {
		type: Number,
		default: 0
	},
	Created_date: {
		type: Date,
		default: Date.now
	},
	product_image: {
		type: [{
			type:  Schema.Types.Mixed,
			default: {
				big: '/img/default_img-big.gif',
				thumbnail: '/img/default_img-thumbnail.gif'
			}
		}],
		default: {
			big: '/img/default_img-big.gif',
			thumbnail: '/img/default_img-thumbnail.gif'
		}
	},
	availability: {
		type: Number,
		default: 1
	}
});

module.exports = mongoose.model('Products', ProductSchema);