'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var OrderSchema = new Schema({
	customer: {
		type: ObjectId,
		ref: 'CustomerSchema'
	},
	products: {
		type: [{
			type:  Schema.Types.Mixed,
			default: {
				product: { type: ObjectId, ref: 'ProductSchema'},
				quantity: {type: Number, default: 0},
				price: {type: Number, default: 0}
			}
		}],
	},
	shipping_address: {
		type: [{
			type:  Schema.Types.Mixed,
			default: {
				city: { type: String},
				address: {type: String},
				telephone: {type: String}
			}
		}],
	},
	credit_card: {
		type: [{
			type:  Schema.Types.Mixed,
			default: {
				owner: { type: String }, 
				number: { type: String },
				expiration_date: { type: String },
				ccv: { type: String }
			}
		}],
	},
	delivery_notes: {
		type: String
	},
	total: {
		type: Number,
		default: 0
	},
	taxes: {
		type: Number,
		default: 0
	},
	Created_date: {
		type: Date,
		default: Date.now
	},
	active: {
		type: Number,
		default: 0
	}

});

module.exports = mongoose.model('Orders', OrderSchema);