'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var CustomerSchema = new Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	email: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true,
		unique: true
	},
	addresses: {
		type: [{
			type:  Schema.Types.Mixed,
			default: {
				city: { type: String},
				address: {type: String},
				telephone: {type: String}
			}
		}],
	},
	credit_cards: {
		type: [{
			type:  Schema.Types.Mixed,
			default: {
				owner: { type: String }, 
				number: { type: String },
				expiration_date: { type: String },
				ccv: { type: String }
			}
		}],
	},
	Created_date: {
		type: Date,
		default: Date.now
	},
	active: {
		type: Number,
		default: 0
	}

});

module.exports = mongoose.model('Customers', CustomerSchema);