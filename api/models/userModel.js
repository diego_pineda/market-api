'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var UserSchema = new Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	email: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true,
		unique: true
	},
	admin: {
		type: Boolean,
		default: false
	},
	Created_date: {
		type: Date,
		default: Date.now
	},
	active: {
		type: Number,
		default: 1
	}

});

module.exports = mongoose.model('Users', UserSchema);