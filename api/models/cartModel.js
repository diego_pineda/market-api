'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var CartSchema = new Schema({
	customer: {
		type: ObjectId,
		ref: 'CustomerSchema'
	},
	products: {
		type: [{
			type:  Schema.Types.Mixed,
			default: {
				product: { type: ObjectId, ref: 'ProductSchema'},
				quantity: {type: Number, default: 0},
				price: {type: Number, default: 0}
			}
		}],
	},
	address: {
		type: ObjectId,
		ref: 'AddressSchema'
	},
	delivery_notes: {
		type: String
	},
	total: {
		type: Number,
		default: 0
	},
	taxes: {
		type: Number,
		default: 0
	},
	Created_date: {
		type: Date,
		default: Date.now
	},
	active: {
		type: Number,
		default: 0
	}

});

module.exports = mongoose.model('Carts', CartSchema);