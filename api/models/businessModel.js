'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BusinessSchema = new Schema({
	name: {
		type: String,
		Required: 'Kindly enter the name of the business owner',
		default: null
	},
	direction: {
		type: String,
		Required: 'enter the direction  of the owner ',
		default: null
	},
	telephone: {
		type: String,
		Required: 'Enter the phone business contact',
		default: null
	},
	taxes: {
		type: Number,
		default: 0
	},
	Created_date: {
		type: Date,
		default: Date.now
	},
	active: {
		type: Number,
		default: 0
	}
});

module.exports = mongoose.model('Business', BusinessSchema);