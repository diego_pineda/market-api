'use strict';

var mongoose = require('mongoose'),
	Customer = mongoose.model('Customers');

exports.list_all_customers = function(req, res) {
	var page = helper.pagination(req, res).page,
		perPage = helper.pagination(req, res).perPage;
	Customer.find()
		    .limit(perPage)
		    .skip(perPage * page)
		    .sort({
		        name: 'asc'
		    })
	.exec(function(err, data) {
		
		if (err) {
			res.send(err);
		}
		
        responser.JSONresponse(req, res, data, Customer);		
	});
};

exports.create_a_customer = function(req, res) { 
	req.body.password = helper.encrypt(req.body.password);
	
	res.json(req.body);
	return false;
	var new_customer = new Customer(req.body);
	new_customer.save(function(err, customer) {
		if (err)
			res.send(err);
		res.json(customer);
	});
};

exports.read_a_customer = function(req, res) {
	Customer.findById(req.params.customerId, function(err, customer) {
		if (err)
			res.send(err);
		res.json(customer);
	});
};

exports.update_a_customer = function(req, res) {
	if(req.body.password) {
		req.body.password = helper.encrypt(req.body.password);
	}
	Customer.findOneAndUpdate(req.params.customerId, req.body, {new: true}, function(err, customer) {
		if (err)
			res.send(err);
		res.json(customer);
	});
};

exports.delete_a_customer = function(req, res) {
	Customer.remove({
		_id: req.params.customerId
	}, function(err, customer) {
		if (err)
			res.send(err);
		res.json({ message: 'Customer successfully deleted' });
	});
};

exports.validate_customer_data = function (req, res, next) {
  	req.checkBody('name', 'Invalid name').notEmpty();
  	req.checkBody('email', 'Email can\'t be empty').notEmpty();
  	req.checkBody('password', 'password can\'t be empty').notEmpty();

  	var errors = req.validationErrors();
	  if (errors) {
	    var response = { errors: [] };
	    errors.forEach(function(err) {
	    	response.errors.push(err.msg);
	    });
	    res.statusCode = 400;
	    return res.json(response);
	  }
  return next();
};