'use strict';

var mongoose = require('mongoose'),
	User = mongoose.model('Users');

exports.list_all_users = function(req, res) {
	var page = helper.pagination(req, res).page,
		perPage = helper.pagination(req, res).perPage;
	User.find()
		    .limit(perPage)
		    .skip(perPage * page)
		    .sort({
		        name: 'asc'
		    })
	.exec(function(err, data) {
		
		if (err) {
			res.send(err);
		}
		
        responser.JSONresponse(req, res, data, User);		
	});
};

exports.create_a_user = function(req, res) { 
	req.body.password = helper.encrypt(req.body.password);
	var new_user = new User(req.body);
	new_user.save(function(err, user) {
		if (err)
			res.send(err);
		res.json(user);
	});
};

exports.read_a_user = function(req, res) {
	User.findById(req.params.userId, function(err, user) {
		if (err)
			res.send(err);
		res.json(user);
	});
};

exports.update_a_user = function(req, res) {
	if(req.body.password) {
		req.body.password = helper.encrypt(req.body.password);
	}
	User.findOneAndUpdate(req.params.userId, req.body, {new: true}, function(err, user) {
		if (err)
			res.send(err);
		res.json(user);
	});
};

exports.delete_a_user = function(req, res) {
	User.remove({
		_id: req.params.userId
	}, function(err, user) {
		if (err)
			res.send(err);
		res.json({ message: 'User successfully deleted' });
	});
};

exports.validate_user_data = function (req, res, next) {
  	req.checkBody('name', 'Invalid name').notEmpty();
  	req.checkBody('email', 'Email can\'t be empty').notEmpty();
  	req.checkBody('password', 'password can\'t be empty').notEmpty();

  	var errors = req.validationErrors();
	  if (errors) {
	    var response = { errors: [] };
	    errors.forEach(function(err) {
	    	response.errors.push(err.msg);
	    });
	    res.statusCode = 400;
	    return res.json(response);
	  }
  return next();
};