'use strict';

var mongoose = require('mongoose'),
	Product = mongoose.model('Products');

exports.list_all_products = function(req, res) {
	var page = helper.pagination(req, res).page,
		perPage = helper.pagination(req, res).perPage;
	Product.find()
		    .limit(perPage)
		    .skip(perPage * page)
		    .sort({
		        name: 'asc'
		    })
	.exec(function(err, data) {
		
		if (err) {
			res.send(err);
		}
		
        responser.JSONresponse(req, res, data, Product);		
	});
};

exports.create_a_product = function(req, res) {
	var new_product = new Product(req.body);
	new_product.save(function(err, product) {
		if (err)
			res.send(err);
		res.json(product);
	});
};

exports.read_a_product = function(req, res) {
	Product.findById(req.params.productId, function(err, product) {
		if (err)
			res.send(err);
		res.json(product);
	});
};

exports.update_a_product = function(req, res) {
	Product.findOneAndUpdate(req.params.productId, req.body, {new: true}, function(err, product) {
		if (err)
			res.send(err);
		res.json(product);
	});
};

exports.delete_a_product = function(req, res) {
	Product.remove({
		_id: req.params.productId
	}, function(err, product) {
		if (err)
			res.send(err);
		res.json({ message: 'Product successfully deleted' });
	});
};

exports.validate_product_data = function (req, res, next) {
  	req.checkBody('name', 'Invalid name').notEmpty();
  	req.checkBody('description', 'Invalid description').notEmpty();
  	req.checkBody('sku', 'SKU can´t be empty').notEmpty();
  	req.checkBody('price', 'Invalid price, must be numeric and it can have decimal format (.)').isFloat();

  	var errors = req.validationErrors();
	  if (errors) {
	    var response = { errors: [] };
	    errors.forEach(function(err) {
	    	response.errors.push(err.msg);
	    });
	    res.statusCode = 400;
	    return res.json(response);
	  }
  return next();
};