'use strict';

var mongoose = require('mongoose'),
	Order = mongoose.model('Orders');

exports.list_all_orders = function(req, res) {
	var page = helper.pagination(req, res).page,
		perPage = helper.pagination(req, res).perPage;
	Order.find()
		    .limit(perPage)
		    .skip(perPage * page)
		    .sort({
		        name: 'asc'
		    })
	.exec(function(err, data) {
		
		if (err) {
			res.send(err);
		}
		
        responser.JSONresponse(req, res, data, Order);		
	});
};

exports.create_a_order = function(req, res) {
	var new_order = new Order(req.body);
	new_order.save(function(err, order) {
		if (err)
			res.send(err);
		res.json(order);
	});
};

exports.read_a_order = function(req, res) {
	Order.findById(req.params.orderId, function(err, order) {
		if (err)
			res.send(err);
		res.json(order);
	});
};

exports.update_a_order = function(req, res) {
	Order.findOneAndUpdate(req.params.orderId, req.body, {new: true}, function(err, order) {
		if (err)
			res.send(err);
		res.json(order);
	});
};

exports.delete_a_order = function(req, res) {
	Order.remove({
		_id: req.params.orderId
	}, function(err, order) {
		if (err)
			res.send(err);
		res.json({ message: 'Order successfully deleted' });
	});
};

exports.validate_order_data = function (req, res, next) {
  	// req.checkBody('customer', 'invalid customer').notEmpty();
  	// req.checkBody('description', 'Invalid description').notEmpty();
  	// req.checkBody('sku', 'SKU can´t be empty').notEmpty();
  	// req.checkBody('price', 'Invalid price, must be numeric and it can have decimal format (.)').isFloat();

  	var errors = req.validationErrors();
	  if (errors) {
	    var response = { errors: [] };
	    errors.forEach(function(err) {
	    	response.errors.push(err.msg);
	    });
	    res.statusCode = 400;
	    return res.json(response);
	  }
  return next();
};