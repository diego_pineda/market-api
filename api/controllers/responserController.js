'use strict';

//var globalSettings = require('globalSettings') ;

exports.JSONresponse = function (req, res, data, model) {
	var page = helper.pagination(req, res).page,
		perPage = helper.pagination(req, res).perPage;
		var collectionName = model.collection.collectionName,
		count = model.count().exec(function(err, count) {
			var nextPage,
				previousPage;
			var queryObj = req.query;
				
			if(typeof queryObj.page != undefined){
				queryObj.page = page + 1;
				
				var queryString = Object.keys(queryObj).map(function(k) {
				    return encodeURIComponent(k) + '=' + encodeURIComponent(queryObj[k])
				}).join('&');
				nextPage = req.path + '?' + queryString;
				if (isNaN(queryObj.page)){
					nextPage = null;
				}

				queryObj.page = page-1;
				var queryString = Object.keys(queryObj).map(function(k) {
				    return encodeURIComponent(k) + '=' + encodeURIComponent(queryObj[k])
				}).join('&');
				previousPage = req.path + '?' + queryString;	
				if (isNaN(queryObj.page)){
					previousPage = null;
				}
			}

			
			res.json({
	            rows: data,
	            page: page,
	            pages: Math.floor(count / perPage),
	            count: count,
	            url: {
	            	actual: req.originalUrl,
	            	next: (page + 1 > Math.floor(count / perPage)) ? null: nextPage,
	            	previous: (page-1 < 0) ? null: previousPage
	            }
	        });		
		});	
}