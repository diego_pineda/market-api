'use strict';


var mongoose = require('mongoose'),
	Business = mongoose.model('Business');

exports.list_all_business = function(req, res) {
	var page = helper.pagination(req, res).page,
		perPage = helper.pagination(req, res).perPage;
	Business.findOne().sort('-created_at').exec(function(err, data) {
		if (err)
      		res.send(err);
    
    	responser.JSONresponse(req, res, data, Business);
	});		    
};

exports.create_a_business = function(req, res) {
	var new_business = new Business(req.body);
	new_business.save(function(err, business) {
		if (err)
			res.send(err);
		res.json(business);
	});
};

exports.read_a_business = function(req, res) {

	//  _id : 591fd7c0bbb41e1d2c73dacb
	Business.findById(req.params.businessId, function(err, business) {
		if (err)
			res.send(err);
		res.json(business);
	});
};

exports.update_a_business = function(req, res) {
	Business.findOneAndUpdate(req.params.businessId, req.body, {new: true}, function(err, business) {
		if (err)
			res.send(err);
		res.json(business);
	});
};


exports.validate_business_data = function (req, res, next) {
  	req.checkBody('name', 'Invalid business name').notEmpty();
  	req.checkBody('direction', 'Invalid direction').notEmpty();
  	req.checkBody('telephone', 'invalid telephone number').notEmpty();
  	req.checkBody('taxes', 'Invalidtax value, must be numeric and it can have decimal format (.)').isFloat();

  	var errors = req.validationErrors();
	  if (errors) {
	    var response = { errors: [] };
	    errors.forEach(function(err) {
	    	response.errors.push(err.msg);
	    });
	    res.statusCode = 400;
	    return res.json(response);
	  }
  return next();
};