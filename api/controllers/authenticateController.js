'use strict';

var mongoose = require('mongoose'),
	User = mongoose.model('Users');

exports.get_token = function (req, res) {
	// find the user 
	User.findOne({
		email: req.body.email
	}, function(err, user) {
		if (err) throw err;
		if (!user) {
        	res.json({ success: false, message: 'Authentication failed. email or password is wrong.' });
        } else if (user) {
	      	// check if password matches
	      	if (user.password != helper.encrypt(req.body.password)) {
	        	res.json({ success: false, message: 'Authentication failed. Wrong password.' });
	      	} else {
		        var token = jwt.sign(user, config.secret, {
		        	expiresIn : 3600 * 24 // expires in 1 hour
		        });

		        res.json({
					success: true,
					message: 'user token returned',
					token: token
		        });
		    }   
	    }
  });
};

exports.verify = function(req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  // decode token
  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, config.secret, function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;    
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });

  }
}