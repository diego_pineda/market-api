'use strict';

var mongoose = require('mongoose'),
	Cart = mongoose.model('Carts');
	
exports.list_all_carts = function(req, res) {
	var page = helper.pagination(req, res).page,
		perPage = helper.pagination(req, res).perPage;
	Cart.find()
		    .limit(perPage)
		    .skip(perPage * page)
		    .sort({
		        name: 'asc'
		    })
	.exec(function(err, data) {
		
		if (err) {
			res.send(err);
		}
		
        responser.JSONresponse(req, res, data, Cart);		
	});
};

exports.create_a_cart = function(req, res) {
	var new_cart = new Cart(req.body);
	new_cart.save(function(err, cart) {
		if (err)
			res.send(err);
		res.json(cart);
	});
};

exports.read_a_cart = function(req, res) {
	Cart.findById(req.params.cartId, function(err, cart) {
		if (err)
			res.send(err);
		res.json(cart);
	});
};

exports.update_a_cart = function(req, res) {
	Cart.findOneAndUpdate(req.params.cartId, req.body, {new: true}, function(err, cart) {
		if (err)
			res.send(err);
		res.json(cart);
	});
};

exports.delete_a_cart = function(req, res) {
	Cart.remove({
		_id: req.params.cartId
	}, function(err, cart) {
		if (err)
			res.send(err);
		res.json({ message: 'Cart successfully deleted' });
	});
};

exports.validate_cart_data = function (req, res, next) {
  	// req.checkBody('items', 'Invalid cart items').notEmpty();
  	// req.checkBody('description', 'Invalid description').notEmpty();
  	// req.checkBody('sku', 'SKU can´t be empty').notEmpty();
  	// req.checkBody('price', 'Invalid price, must be numeric and it can have decimal format (.)').isFloat();

  	var errors = req.validationErrors();
	  if (errors) {
	    var response = { errors: [] };
	    errors.forEach(function(err) {
	    	response.errors.push(err.msg);
	    });
	    res.statusCode = 400;
	    return res.json(response);
	  }
  return next();
};