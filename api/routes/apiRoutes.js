'use strict';
module.exports = function(app) {
 	var products = require('../controllers/productController');
    var orders = require('../controllers/orderController');
    var carts = require('../controllers/cartController');
    var customers = require('../controllers/customerController');
    var users = require('../controllers/userController');
    var authenticate = require('../controllers/authenticateController');
 	var business = require('../controllers/businessController');

    
    // auth Routes
    app.route('/api/authenticate')
        .post(authenticate.get_token);
        
    // route middleware to verify a token
    app.use('/api',authenticate.verify);

    /*begins api routes*/
    app.get('/api', function(req, res) {
      res.json({ message: 'Welcome to the market-app RESTful API' });
    });


    // product Routes 
    app.route('/api/products')
    	.post(products.validate_product_data); //validate create data for product
  	app.route('/api/products')
    	.get(products.list_all_products)
    	.post(products.create_a_product);
	
	app.route('/api/products/:productId')
		.get(products.read_a_product)
    	.put(products.update_a_product)
    	.delete(products.delete_a_product);

    // order Routes 
    app.route('/api/orders')
        .post(orders.validate_order_data); //validate create data for an order
    app.route('/api/orders')
        .get(orders.list_all_orders)
        .post(orders.create_a_order);
    
    app.route('/api/orders/:orderId')
        .get(orders.read_a_order)
        .put(orders.update_a_order)
        .delete(orders.delete_a_order);

    // cart Routes 
    app.route('/api/carts')
        .post(carts.validate_cart_data); //validate create data for a cart
    app.route('/api/carts')
        .get(carts.list_all_carts)
        .post(carts.create_a_cart);
    
    app.route('/api/carts/:cartId')
        .get(carts.read_a_cart)
        .put(carts.update_a_cart)
        .delete(carts.delete_a_cart);

    // customer Routes 
    app.route('/api/customers')
        .post(customers.validate_customer_data); //validate create data for customer
    app.route('/api/customers')
        .get(customers.list_all_customers)
        .post(customers.create_a_customer);
    
    app.route('/api/customers/:customerId')
        .get(customers.read_a_customer)
        .put(customers.update_a_customer)
        .delete(customers.delete_a_customer);

    // user Routes 
    app.route('/api/users')
        .post(users.validate_user_data); //validate create data for user
    app.route('/api/users')
        .get(users.list_all_users)
        .post(users.create_a_user);
    
    app.route('/api/users/:userId')
        .get(users.read_a_user)
        .put(users.update_a_user)
        .delete(users.delete_a_user);



    // business Routes 
    app.route('/api/business')
    	.post(business.validate_business_data); //validate create data
    app.route('/api/business/:businessId')
		.put(business.validate_business_data);  //validate update data

  	app.route('/api/business')
    	.get(business.list_all_business)
    	.post(business.create_a_business);
	
	app.route('/api/business/:businessId')
		.get(business.read_a_business)
    	.put(business.update_a_business);
};  

